#include "board.h"
#include "board-config.h"
#include "delay.h"
#include "drivers/battery.h"
#include "rtc-board.h"
#include "spiFlash.h"
#include "sx126x-board.h"
#include <drivers/SpiMaster.h>
#include <nrf_drv_clock.h>
#include <nrf_log_ctrl.h>
#include <nrf_log_default_backends.h>
#include <nrfx_gpiote.h>

void EepromMcuInit(void);

uint8_t uniqueId[8] = {0, 0, 0, 0, 0, 0, 0, 0};

void BoardInitMcu(void) {
  // Enable internal DCDC converter, improves power usage
  NRF_POWER->DCDCEN = 1;

  // Init clocks
  nrf_drv_clock_init();
  nrf_drv_clock_lfclk_request(NULL);
  nrfx_clock_lfclk_start();
  while (!nrf_clock_lf_is_running()) {
  }

  // Init logger
  NRF_LOG_INIT(NULL);
  NRF_LOG_DEFAULT_BACKENDS_INIT();

  // Init Gpiote module
  nrfx_gpiote_init();

  // Init non volatile memory (implemented as a LittleFS file in external (SPI) flash memory
  EepromMcuInit();

  // Init SX1262 (GPIO and SPI)
  SX126xIoInit();

  SX126x.Spi.Nss.pin = RADIO_NSS;
  nrf_gpio_pin_set(RADIO_NSS);
  nrf_gpio_cfg_output(RADIO_NSS);
  nrf_gpio_cfg_output(RADIO_RESET);
  SpiInit(&SX126x.Spi, SPI_1, SPI_MOSI, SPI_MISO, SPI_CLK, RADIO_NSS);

  // Init RTC
  RtcInit();
}

void BoardResetMcu(void) {

}

void BoardInitPeriph(void) {

}

uint8_t BoardGetBatteryLevel(void) {
  return battery_getVoltage();
}

uint32_t BoardGetRandomSeed(void) {
  return 0;
}

void BoardGetUniqueId(uint8_t *id) {
  id[0] = uniqueId[0];
  id[1] = uniqueId[1];
  id[2] = uniqueId[2];
  id[3] = uniqueId[3];
  id[4] = uniqueId[4];
  id[5] = uniqueId[5];
  id[6] = uniqueId[6];
  id[7] = uniqueId[7];
}

extern struct SpiMasterContext loraSpiContext;
void BoardLowPowerHandler(void) {
  // Put the SPI flash and SPI peripheral to sleep
  SpiFlash_Sleep();
  SpiMaster_sleep(&loraSpiContext);

  // Put the CPU to sleep
  __WFI();

  // CPU has just woken up, wake the SPI peripheral and flash memory up
  SpiMaster_wakeup(&loraSpiContext);
  SpiFlash_Wakeup();
}

void BoardCriticalSectionBegin( uint32_t *mask ) {
  __disable_irq();
}

void BoardCriticalSectionEnd( uint32_t *mask ) {
  __enable_irq();
}
