#include "delay-board.h"
#include <nrf_delay.h>


void DelayMsMcu(uint32_t ms) {
  nrf_delay_ms(ms);
}