#ifndef __BOARD_CONFIG_H__
#define __BOARD_CONFIG_H__

#ifdef __cplusplus
extern "C"
{
#endif

#define RADIO_NSS 5
#define RADIO_BUSY 29
#define RADIO_DIO_1 28
#define RADIO_RESET 4
#define SPI_MOSI (32+15) // P1.15
#define SPI_MISO (32+14) // P1.14
#define SPI_CLK (32+13) // P1.13
#define RADIO_ANT_SWITCH_POWER PA_5


/*!
 * Defines the time required for the TCXO to wakeup [ms].
 */
#define BOARD_TCXO_WAKEUP_TIME                      0

#ifdef __cplusplus
}
#endif

#endif // __BOARD_CONFIG_H__