#include "gpio-board.h"
#include "board-config.h"
#include "rtc-board.h"
#include <nrf_delay.h>
#include <nrf_gpio.h>
#include <nrf_log.h>
#include <nrfx_gpiote.h>

bool reset_alredy_init = 0;
nrfx_gpiote_in_config_t pinConfigs[32];
GpioIrqHandler* pinIrqHandlers[32] = {NULL};

void irq(nrfx_gpiote_pin_t pin, nrf_gpiote_polarity_t action) {
  if(pinIrqHandlers[pin] != NULL)
    pinIrqHandlers[pin](NULL);
}

void GpioMcuInit(Gpio_t *obj, PinNames pin, PinModes mode, PinConfigs config, PinTypes type, uint32_t value) {
  obj->pin = pin;

  if(obj->pin == RADIO_RESET && reset_alredy_init) {
    if(mode == PIN_OUTPUT)
      GpioMcuWrite(obj, 0);
    else
      GpioMcuWrite(obj, 1);

    return;
  }

  nrf_gpio_pin_pull_t pull;
  switch(type) {
    case PIN_NO_PULL: pull = NRF_GPIO_PIN_NOPULL; break;
    case PIN_PULL_UP: pull = NRF_GPIO_PIN_PULLUP; break;
    case PIN_PULL_DOWN: pull = NRF_GPIO_PIN_PULLDOWN; break;
  }

  if(mode == PIN_INPUT) {
    nrfx_gpiote_in_config_t pinConfig;
    pinConfig.skip_gpio_setup = false;
    pinConfig.hi_accuracy = false;
    pinConfig.is_watcher = false;
    pinConfig.sense = 0;
    pinConfig.pull = pull;
    nrfx_gpiote_in_init(obj->pin, &pinConfig, NULL);

    pinConfigs[pin] = pinConfig;
  } else if (mode == PIN_OUTPUT) {
    nrf_gpio_cfg_output(pin);
  } else if (mode == PIN_ALTERNATE_FCT) {
  } else if(mode == PIN_ANALOGIC) {
  }

  if(obj->pin == RADIO_RESET) {
    reset_alredy_init = true;
  }
}

void GpioMcuSetContext(Gpio_t *obj, void* context) {
  obj->Context = context;
}

void GpioMcuSetInterrupt(Gpio_t *obj, IrqModes irqMode, IrqPriorities irqPriority, GpioIrqHandler *irqHandler) {
  nrf_gpiote_polarity_t polarity;
  switch(irqMode) {
    case IRQ_RISING_EDGE: polarity = NRF_GPIOTE_POLARITY_LOTOHI; break;
    case IRQ_FALLING_EDGE: polarity = NRF_GPIOTE_POLARITY_HITOLO; break;
    case IRQ_RISING_FALLING_EDGE: polarity = NRF_GPIOTE_POLARITY_TOGGLE; break;
  }

  if(irqMode != NO_IRQ) {
    nrfx_gpiote_in_uninit(obj->pin);

    nrfx_gpiote_in_config_t pinConfig = pinConfigs[obj->pin];
    pinConfig.sense = polarity;
    pinConfig.hi_accuracy = true;
    pinConfig.skip_gpio_setup = false;

    nrfx_gpiote_in_init(obj->pin, &pinConfig, irq);
    nrfx_gpiote_in_event_enable(obj->pin, true);

    if(irqHandler != NULL)
      pinIrqHandlers[obj->pin] = irqHandler;
  }
}

void GpioMcuRemoveInterrupt(Gpio_t *obj) {
  nrfx_gpiote_in_config_t pinConfig = pinConfigs[obj->pin];
  pinConfig.skip_gpio_setup = false;
  pinConfig.hi_accuracy = false;
  pinConfig.is_watcher = false;
  pinConfig.sense = 0;
  pinConfig.pull = NRF_GPIO_PIN_PULLDOWN;
  nrfx_gpiote_in_init(obj->pin, &pinConfig, irq);
  nrfx_gpiote_in_event_enable(obj->pin, false);
}

void GpioMcuWrite(Gpio_t *obj, uint32_t value) {
  nrf_gpio_pin_write(obj->pin, value);
}

void GpioMcuToggle(Gpio_t *obj) {
  nrf_gpio_pin_toggle(obj->pin);
}

uint32_t GpioMcuRead(Gpio_t *obj) {
  return nrf_gpio_pin_read(obj->pin);
}