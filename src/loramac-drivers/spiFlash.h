#ifndef XIAO_LORA_WATER_METER_SPIFLASH_H
#define XIAO_LORA_WATER_METER_SPIFLASH_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint-gcc.h>

struct __attribute__((packed)) Identification {
  uint8_t manufacturer;
  uint8_t type;
  uint8_t density;
};

enum Commands {
  PageProgram = 0x02,
  Read = 0x03,
  ReadStatusRegister = 0x05,
  WriteEnable = 0x06,
  ReadConfigurationRegister = 0x15,
  SectorErase = 0x20,
  ReadSecurityRegister = 0x2B,
  ReadIdentification = 0x9F,
  ReleaseFromDeepPowerDown = 0xAB,
  DeepPowerDown = 0xB9
};

void SpiFlash_Init();
struct Identification SpiFlash_ReadIdentification();

uint8_t SpiFlash_ReadStatusRegister();
bool SpiFlash_WriteInProgress();
bool SpiFlash_WriteEnabled();
uint8_t SpiFlash_ReadConfigurationRegister();
void SpiFlash_Read(uint32_t address, uint8_t* buffer, size_t size);
void SpiFlash_Write(uint32_t address, const uint8_t* buffer, size_t size);
void SpiFlash_WriteEnable();
void SpiFlash_SectorErase(uint32_t sectorAddress);
uint8_t SpiFlash_ReadSecurityRegister();
bool SpiFlash_ProgramFailed();
bool SpiFlash_EraseFailed();

void SpiFlash_Sleep();
void SpiFlash_Wakeup();


#endif//XIAO_LORA_WATER_METER_SPIFLASH_H
