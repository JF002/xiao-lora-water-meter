#include "rtc-board.h"
#include "utilities.h"
#include <nrf_drv_rtc.h>
#include <nrf_log.h>

uint64_t rtcOffset;
uint64_t rtcAlarm;
uint64_t rtcTimerContext;
bool alarmRunning = false;
uint32_t lastTimerValue = 0;
uint32_t calendarTimeOffset = 0;


static void rtc_handler(nrf_drv_rtc_int_type_t int_type) {
  if (int_type == NRF_DRV_RTC_INT_COMPARE0) {
    TimerIrqHandler( );
  }
}

/*!
 * \brief Initializes the RTC timer
 *
 * \remark The timer is based on the RTC
 */
const nrf_drv_rtc_t rtc = NRF_DRV_RTC_INSTANCE(0);

void RtcInit(void) {
  uint32_t err_code;

  //Initialize RTC instance
  nrf_drv_rtc_config_t config = NRF_DRV_RTC_DEFAULT_CONFIG;
  config.prescaler = 31; // 32 = 1024 ticks per second - 0 = 32768 ticks per second
  err_code = nrf_drv_rtc_init(&rtc, &config, rtc_handler);
  APP_ERROR_CHECK(err_code);

  //Power on RTC instance
  nrf_drv_rtc_enable(&rtc);
}

/*!
 * \brief Returns the minimum timeout value
 *
 * \retval minTimeout Minimum timeout value in in ticks
 */
uint32_t RtcGetMinimumTimeout(void) {
  return 3;
}

/*!
 * \brief converts time in ms to time in ticks
 *
 * \param[IN] milliseconds Time in milliseconds
 * \retval returns time in timer ticks
 */
uint32_t RtcMs2Tick(TimerTime_t milliseconds) {
  uint32_t v = ( uint32_t )( ( ( ( uint64_t )milliseconds ) << 10 ) / 1000 );
  return v;
}

/*!
 * \brief converts time in ticks to time in ms
 *
 * \param[IN] time in timer ticks
 * \retval returns time in milliseconds
 */
TimerTime_t RtcTick2Ms(uint32_t tick) {
  uint32_t seconds = tick >> 10;

  tick = tick & 0x3FF;
  return ( ( seconds * 1000 ) + ( ( tick * 1000 ) >> 10 ) );
}

/*!
 * \brief Sets the alarm
 *
 * \note The alarm is set at now (read in this funtion) + timeout
 *
 * \param timeout [IN] Duration of the Timer ticks
 */
void RtcSetAlarm(uint32_t timeout) {
  nrf_drv_rtc_cc_set(&rtc,0,timeout + rtcTimerContext,true);
}

/*!
 * \brief Stops the Alarm
 */
void RtcStopAlarm(void) {
  // TODO, probably
}

/*!
 * \brief Sets the RTC timer reference
 *
 * \retval value Timer reference value in ticks
 */
uint32_t RtcSetTimerContext(void) {
  rtcTimerContext = RtcGetTimerValue();
  return rtcTimerContext;
}

/*!
 * \brief Gets the RTC timer reference
 *
 * \retval value Timer value in ticks
 */
uint32_t RtcGetTimerContext(void) {
  return rtcTimerContext;
}

/*!
 * \brief Gets the system time with the number of seconds elapsed since epoch
 *
 * \param [OUT] milliseconds Number of milliseconds elapsed since epoch
 * \retval seconds Number of seconds elapsed since epoch
 */
uint32_t RtcGetCalendarTime(uint16_t *milliseconds) {
  uint32_t ticks = 0;

  uint32_t calendarValue = RtcGetTimerValue( );
  calendarValue &= 0xFFFFFF;
  calendarValue += (calendarTimeOffset<<24);

  uint32_t seconds = ( uint32_t )calendarValue >> 10;

  ticks =  ( uint32_t )calendarValue & 0x3FF;

  *milliseconds = RtcTick2Ms( ticks );

  return seconds;
}

/*!
 * \brief Get the RTC timer value
 *
 * \retval RTC Timer value
 */
uint32_t RtcGetTimerValue(void) {
  return nrfx_rtc_counter_get(&rtc);
}

/*!
 * \brief Get the RTC timer elapsed time since the last Alarm was set
 *
 * \retval RTC Elapsed time since the last alarm in ticks.
 */
uint32_t RtcGetTimerElapsedTime(void) {
  uint32_t elapsed = RtcGetTimerValue() -  rtcTimerContext;
  return (elapsed);
}

/*!
 * \brief Writes data0 and data1 to the RTC backup registers
 *
 * \param [IN] data0 1st Data to be written
 * \param [IN] data1 2nd Data to be written
 */
void RtcBkupWrite(uint32_t data0, uint32_t data1) {
}

/*!
 * \brief Reads data0 and data1 from the RTC backup registers
 *
 * \param [OUT] data0 1st Data to be read
 * \param [OUT] data1 2nd Data to be read
 */
void RtcBkupRead(uint32_t* data0, uint32_t* data1) {
  *data0 = 0;
  *data1 = 0;
}

/*!
 * \brief Processes pending timer events
 */


void RtcProcess( void ) {
  CRITICAL_SECTION_BEGIN( );

  uint32_t currentTimerValue = (RtcGetTimerValue()) & 0x00ffffff;
  if(lastTimerValue <= currentTimerValue) {
    lastTimerValue = currentTimerValue;
  } else {
    calendarTimeOffset++; // Must add (1 << 24) to timer value for calendar value
    lastTimerValue = currentTimerValue;
  }
  CRITICAL_SECTION_END( );
}