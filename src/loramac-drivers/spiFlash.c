#include "spiFlash.h"
#include "../drivers/SpiMaster.h"
#include <nrf_delay.h>
#include <nrf_gpio.h>

struct SpiMasterContext spi;
int pinNSS;

void SpiWriteWithoutNss(const uint8_t* data, size_t size) {
  uint16_t dataByte;

  for(int i = 0; i < size; i++) {
    dataByte = data[i];
    SpiMaster_inOut(&spi, dataByte);
  }
}

void SpiIn(uint8_t* buffer, size_t size) {
  uint16_t dummy = 0;
  for(int i = 0; i < size; i++) {
    buffer[i] = SpiMaster_inOut(&spi, dummy);
  }
}

void SpiWrite(const uint8_t* data, size_t size) {
  nrf_gpio_pin_clear(pinNSS);
  SpiWriteWithoutNss(data, size);
  nrf_gpio_pin_set(pinNSS);
}

void SpiRead(const uint8_t* cmd, size_t cmdSize, uint8_t* data, size_t dataSize) {
  nrf_gpio_pin_clear(pinNSS);
  SpiWriteWithoutNss(cmd, cmdSize);

  SpiIn(data, dataSize);
  nrf_gpio_pin_set(pinNSS);

}

void SpiWriteCmdAndBuffer(const uint8_t* cmd, size_t cmdSize, const uint8_t* data, size_t dataSize) {
  nrf_gpio_pin_clear(pinNSS);
  SpiWriteWithoutNss(cmd, cmdSize);
  SpiWriteWithoutNss(data, dataSize);
  nrf_gpio_pin_set(pinNSS);
}

void SpiFlash_Init() {
  pinNSS = 25;
  nrf_gpio_pin_set(pinNSS);
  nrf_gpio_cfg_output(pinNSS);
  SpiMaster_init(&spi, SPI2, SpiMode3, 20, 24, 21);
}

struct Identification SpiFlash_ReadIdentification() {
  uint8_t cmd = ReadIdentification;
  struct Identification identification;
  identification.density = 0;
  identification.type = 0;
  identification.manufacturer = 0;
  SpiRead(&cmd, 1, (uint8_t *) &identification, sizeof(struct Identification));
  // Expected : manufacturer = 133/0x85, type = 96/0x60, density = 21/0x15
  return identification;
}

uint8_t SpiFlash_ReadStatusRegister() {
  uint8_t cmd = ReadStatusRegister;
  uint8_t status;
  SpiRead(&cmd, 1, &status, 1);
  return status;
}

bool SpiFlash_WriteInProgress() {
  return (SpiFlash_ReadStatusRegister() & 0x01u) == 0x01u;
}

bool SpiFlash_WriteEnabled() {
  return (SpiFlash_ReadStatusRegister() & 0x02u) == 0x02u;
}

uint8_t SpiFlash_ReadConfigurationRegister() {

}

void SpiFlash_Read(uint32_t address, uint8_t* buffer, size_t size) {
  uint8_t cmd[4] = {
    Read,
    address >> 16,
    address >> 8,
    address
  };
  SpiRead(cmd, 4, buffer, size);
}

const uint16_t pageSize = 4096;

void SpiFlash_Write(uint32_t address, const uint8_t* buffer, size_t size) {
  static const uint8_t cmdSize = 4;

  size_t len = size;
  uint32_t addr = address;
  const uint8_t* b = buffer;
  while (len > 0) {
    uint32_t pageLimit = (addr & ~(pageSize - 1u)) + pageSize;
    uint32_t toWrite = pageLimit - addr > len ? len : pageLimit - addr;

    uint8_t cmd[4] = {PageProgram,
                            addr >> 16U,
                            addr >> 8U,
                            addr};

    SpiFlash_WriteEnable();
    while (!SpiFlash_WriteEnabled())
      nrf_delay_ms(1);

    SpiWriteCmdAndBuffer(cmd, cmdSize, b, toWrite);

    while (SpiFlash_WriteInProgress())
      nrf_delay_ms(1);

    addr += toWrite;
    b += toWrite;
    len -= toWrite;
  }
}

void SpiFlash_WriteEnable() {
  uint8_t cmd = WriteEnable;
  SpiWrite(&cmd, 1);
}

void SpiFlash_SectorErase(uint32_t sectorAddress) {
  static const uint8_t cmdSize = 4;
  uint8_t cmd[4] = {SectorErase,
                          sectorAddress >> 16U,
                          sectorAddress >> 8U,
                          sectorAddress};

  SpiFlash_WriteEnable();
  while (!SpiFlash_WriteEnabled())
    nrf_delay_ms(1);

  SpiRead(cmd, cmdSize, NULL, 0);

  while (SpiFlash_WriteInProgress())
    nrf_delay_ms(1);
}

uint8_t SpiFlash_ReadSecurityRegister() {

}

bool SpiFlash_ProgramFailed() {

}

bool SpiFlash_EraseFailed() {

}

void SpiFlash_Sleep() {
  uint8_t outData = DeepPowerDown;
  SpiWrite(&outData, 1);
  SpiMaster_sleep(&spi);
}

void SpiFlash_Wakeup() {
  SpiMaster_wakeup(&spi);
  uint8_t cmd[4] = {ReleaseFromDeepPowerDown, 0x01, 0x02, 0x03};
  SpiWrite(cmd, 1);
}
