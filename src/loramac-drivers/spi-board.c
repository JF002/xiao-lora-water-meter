#include "spi-board.h"
#include "gpio.h"
#include <drivers/SpiMaster.h>

extern struct SpiMasterContext loraSpiContext; // TODO do not use those extern global variable (maybe add the context in Spi_t?)

void SpiInit(Spi_t *obj, SpiId_t spiId, PinNames mosi, PinNames miso, PinNames sclk, PinNames nss) {
  enum SpiMasterModule spiModule;
  switch(spiId) {
    case SPI_1: spiModule = SPI1; break;
    case SPI_2: spiModule = SPI2; break;
  }

  SpiMaster_init(&loraSpiContext, spiModule, SpiMode0, mosi, miso, sclk);
}

void SpiDeInit(Spi_t *obj) {
  loraSpiContext.baseAddress->ENABLE = 0;
}

void SpiFormat(Spi_t *obj, int8_t bits, int8_t cpol, int8_t cpha, int8_t slave) {
  // TODO
}

void SpiFrequency(Spi_t *obj, uint32_t hz) {
  // TODO
}

uint16_t SpiInOut(Spi_t *obj, uint16_t outData) {
  return SpiMaster_inOut(&loraSpiContext, outData);
}

void SpiOut(Spi_t* obj, uint8_t* data, size_t size) {
  SpiMaster_out(&loraSpiContext, data, size);
}
