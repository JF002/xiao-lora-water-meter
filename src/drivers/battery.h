#ifndef XIAO_LORA_WATER_METER_BATTERY_H
#define XIAO_LORA_WATER_METER_BATTERY_H

void battery_init(int batteryVoltagePin, int batteryEnablePin);
float battery_getVoltage();

#endif//XIAO_LORA_WATER_METER_BATTERY_H
