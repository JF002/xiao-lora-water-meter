#include "SpiMaster.h"
#include <nrf_gpio.h>

void SetupWorkaroundForFtpan58(NRF_SPIM_Type* spim, uint32_t ppi_channel, uint32_t gpiote_channel) {
  NRF_GPIOTE->CONFIG[gpiote_channel] = (GPIOTE_CONFIG_MODE_Event << GPIOTE_CONFIG_MODE_Pos) | (spim->PSEL.SCK << GPIOTE_CONFIG_PSEL_Pos) |
                                       (GPIOTE_CONFIG_POLARITY_Toggle << GPIOTE_CONFIG_POLARITY_Pos);

  // Stop the spim instance when SCK toggles.
  NRF_PPI->CH[ppi_channel].EEP = (uint32_t) &NRF_GPIOTE->EVENTS_IN[gpiote_channel];
  NRF_PPI->CH[ppi_channel].TEP = (uint32_t) &spim->TASKS_STOP;
  NRF_PPI->CHENSET = 1U << ppi_channel;
  spim->EVENTS_END = 0;

  // Disable IRQ
  spim->INTENCLR = (1 << 6);
  spim->INTENCLR = (1 << 1);
  spim->INTENCLR = (1 << 19);
}

// TODO WARNING : GPIOTE channel conflict (they are also used by GPIO IRQ)
void DisableWorkaroundForFtpan58(NRF_SPIM_Type* spim, uint32_t ppi_channel, uint32_t gpiote_channel) {
  NRF_GPIOTE->CONFIG[gpiote_channel] = 0;
  NRF_PPI->CH[ppi_channel].EEP = 0;
  NRF_PPI->CH[ppi_channel].TEP = 0;
  NRF_PPI->CHENSET = ppi_channel;
  spim->EVENTS_END = 0;
  spim->INTENSET = (1 << 6);
  spim->INTENSET = (1 << 1);
  spim->INTENSET = (1 << 19);
}

void PrepareTx(NRF_SPIM_Type* spim, const volatile uint32_t outBufferAddress, const volatile uint32_t inBufferAddress, const volatile size_t size) {
  spim->TXD.PTR = outBufferAddress;
  spim->TXD.MAXCNT = size;
  spim->TXD.LIST = 0;
  spim->RXD.PTR = inBufferAddress;
  spim->RXD.MAXCNT = size;
  spim->RXD.LIST = 0;
  spim->EVENTS_END = 0;
}

void SpiMaster_init(struct SpiMasterContext * context, enum SpiMasterModule spiId, enum SpiMasterModes mode, int pinMosi, int  pinMiso, int pinSclk) {
  context->pinClk = pinSclk;
  context->pinMiso = pinMiso;
  context->pinMosi = pinMosi;
  context->spiId = spiId;
  context->mode = mode;

  nrf_gpio_pin_set(pinSclk);
  nrf_gpio_cfg_output(pinSclk);
  nrf_gpio_pin_clear(pinMosi);
  nrf_gpio_cfg_output(pinMosi);
  nrf_gpio_cfg_input(pinMiso, NRF_GPIO_PIN_NOPULL);

  if(spiId == SPI0)
    context->baseAddress = NRF_SPIM0;
  else if(spiId == SPI1)
    context->baseAddress = NRF_SPIM1;
  else if(spiId == SPI2)
    context->baseAddress = NRF_SPIM2;


  /* Configure pins, frequency and mode */
  context->baseAddress->PSELSCK = pinSclk;
  context->baseAddress->PSELMOSI = pinMosi;
  context->baseAddress->PSELMISO = pinMiso;

  context->baseAddress->FREQUENCY = 0x80000000;

  uint32_t regConfig = 0;

  switch (mode) {
    case SpiMode0:
      break;
    case SpiMode1:
      regConfig |= (0x01 << 1);
      break;
    case SpiMode2:
      regConfig |= (0x02 << 1);
      break;
    case SpiMode3:
      regConfig |= (0x03 << 1);
      break;
  }

  context->baseAddress->CONFIG = regConfig;
  context->baseAddress->EVENTS_ENDRX = 0;
  context->baseAddress->EVENTS_ENDTX = 0;
  context->baseAddress->EVENTS_END = 0;

  context->baseAddress->ENABLE = (SPIM_ENABLE_ENABLE_Enabled << SPIM_ENABLE_ENABLE_Pos);

}

uint16_t SpiMaster_inOut(struct SpiMasterContext * context, uint16_t outData )
{
  uint16_t inData;
  SetupWorkaroundForFtpan58(context->baseAddress, 0, 17);


  //nrf_gpio_pin_clear(obj->Nss.pin);
  PrepareTx(context->baseAddress, (uint32_t)&outData, (uint32_t)&inData, 1);

  context->baseAddress->TASKS_START = 1;

  while (context->baseAddress->EVENTS_END == 0);
  //nrf_gpio_pin_set(obj->Nss.pin);
  DisableWorkaroundForFtpan58(context->baseAddress, 0, 17);


  return inData;
}

void SpiMaster_out(struct SpiMasterContext * context, uint8_t* data, size_t size) {
  SetupWorkaroundForFtpan58(context->baseAddress, 0, 17);

  //nrf_gpio_pin_clear(obj->Nss.pin);
  PrepareTx(context->baseAddress, (uint32_t)data, 0, size);

  context->baseAddress->TASKS_START = 1;

  while (context->baseAddress->EVENTS_END == 0);
  //nrf_gpio_pin_set(obj->Nss.pin);
  DisableWorkaroundForFtpan58(context->baseAddress, 0, 17);
}

void SpiMaster_sleep(struct SpiMasterContext * context) {
  while (context->baseAddress->ENABLE != 0) {
    context->baseAddress->ENABLE = (SPIM_ENABLE_ENABLE_Disabled << SPIM_ENABLE_ENABLE_Pos);
  }
  nrf_gpio_cfg_default(context->pinClk);
  nrf_gpio_cfg_default(context->pinMosi);
  nrf_gpio_cfg_default(context->pinMiso);
}

void SpiMaster_wakeup(struct SpiMasterContext * context) {
  SpiMaster_init(context, context->spiId, context->mode, context->pinMosi, context->pinMiso, context->pinClk);
}
