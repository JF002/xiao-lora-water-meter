#ifndef XIAO_LORA_WATER_METER_SPIMASTER_H
#define XIAO_LORA_WATER_METER_SPIMASTER_H

#include <nrf.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint-gcc.h>

enum SpiMasterModule { SPI0, SPI1, SPI2 };
enum SpiMasterModes { SpiMode0, SpiMode1, SpiMode2, SpiMode3 };


struct SpiMasterContext {
  NRF_SPIM_Type* baseAddress;
  int pinMosi;
  int pinMiso;
  int pinClk;
  enum SpiMasterModule spiId;
  enum SpiMasterModes mode;
};

void SpiMaster_init(struct SpiMasterContext * context, enum SpiMasterModule spiId, enum SpiMasterModes mode, int pinMosi, int  pinMiso, int pinSclk);
uint16_t SpiMaster_inOut(struct SpiMasterContext * context, uint16_t outData);
void SpiMaster_out(struct SpiMasterContext * context, uint8_t* data, size_t size);

void SpiMaster_sleep(struct SpiMasterContext * context);
void SpiMaster_wakeup(struct SpiMasterContext * context);

#endif//XIAO_LORA_WATER_METER_SPIMASTER_H
