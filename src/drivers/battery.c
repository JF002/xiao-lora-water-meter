#include "battery.h"
#include <nrf_gpio.h>
#include <nrfx_saadc.h>

float battery_voltage = 0;
int battery_vbatEnablePin = 0;
nrf_saadc_value_t saadc_value;

void AdcCallbackStatic(nrfx_saadc_evt_t const* event) {
  nrf_gpio_pin_set(battery_vbatEnablePin);
  nrfx_saadc_uninit();
  battery_voltage = event->data.done.p_buffer[0] * (((4.0*1510.0)/510.0) *600.0) / 1024.0;
}

void battery_init(int vbatPin, int vbatEnablePin) {
  battery_vbatEnablePin = vbatEnablePin;
  nrfx_saadc_config_t adcConfig = NRFX_SAADC_DEFAULT_CONFIG;
  APP_ERROR_CHECK(nrfx_saadc_init(&adcConfig, AdcCallbackStatic));

  nrf_saadc_channel_config_t adcChannelConfig = {.resistor_p = NRF_SAADC_RESISTOR_DISABLED,
                                                 .resistor_n = NRF_SAADC_RESISTOR_DISABLED,
                                                 .gain = NRF_SAADC_GAIN1_4,
                                                 .reference = NRF_SAADC_REFERENCE_INTERNAL,
                                                 .acq_time = NRF_SAADC_ACQTIME_40US,
                                                 .mode = NRF_SAADC_MODE_SINGLE_ENDED,
                                                 .burst = NRF_SAADC_BURST_ENABLED,
                                                 .pin_p = vbatPin,
                                                 .pin_n = NRF_SAADC_INPUT_DISABLED};

  nrf_gpio_cfg_output(vbatEnablePin);
  nrf_gpio_pin_clear(vbatEnablePin);
  APP_ERROR_CHECK(nrfx_saadc_channel_init(0, &adcChannelConfig));
  APP_ERROR_CHECK(nrfx_saadc_buffer_convert(&saadc_value, 1));
  nrfx_saadc_sample();
}

float battery_getVoltage() {
  return battery_voltage;
}