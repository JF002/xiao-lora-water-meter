# XIAO LoRa water meter

Low-power battery operated LoRaWAN pulse counter, intended to be used as a water meter, to monitor the water usage of my house.

See [this blog post series](https://codingfield.comblog/2023-08/diy-low-power-lora-water-meter-part1/) for more information about this project.

# What is this?
The goal of this project is to monitor the water usage of my house. My water meter (via a additional module) provides a pulse output which generate a pulse when a liter flown through the meter. This project counts the pulses from the meter and send the results over LoRaWAN. 

![](doc/v1-in-case.jpg)

# Hardware

The [PCB](pcb/pcb.fzz) is designed using [Fritzing](https://fritzing.org/).

This project mainly uses 2 boards:

- The [SeeedStudio XIAO NRF52840](https://www.seeedstudio.com/Seeed-XIAO-BLE-nRF52840-p-5201.html?queryID=0d9c5c36e9adc8d83eae6bb05fb98174&objectID=5201&indexName=bazaar_retailer_products) as the main MCU board.
- The [Pine64](https://pine64.org) LoRa module

## Pin mapping

| Xiao NRF52840 | LoRa Module | IN1/2 |
|---------------|-------------|-------|
| GND           | GND         | GND   |
| 3v3           | VCC         |       |
| P1.15         | SPI MOSI    |       |
| P1.14         | SPI MISO    |       |
| P1.13         | SPI CLK     |       |
| P0.02         |             | IN1   |
| P0.03         |             | IN2   |
| P0.28         | DIO1        |       |
| P0.29         | BUSY        |       |
| P0.04         | RESET       |       |
| P0.05         | SPI NSS     |       |

The battery wires are soldered on the battery pads on the bottom of the XIAO board.

# Software

This firmware is written in C and integrated [LoRaMac-Node](https://github.com/Lora-net/LoRaMac-node) and [LitteFS](https://github.com/littlefs-project/littlefs).

## How to build

To build this project, you'll need to download and uncompress

- A cross-compiler : [ARM-GCC (10.3-2021.10)](https://developer.arm.com/downloads/-/gnu-rm)
- The NRF52 SDK 15.3.0 : [nRF-SDK v15.3.0](https://developer.nordicsemi.com/nRF5_SDK/nRF5_SDK_v15.x.x/nRF5_SDK_15.3.0_59ac345.zip)

```shell
$ git clone https://codeberg.org/JF002/xiao-lora-water-meter
$ cd xiao-lora-water-meter
$ git submodule update --init
$ mkdir build && cd build
$ cmake -DARM_NONE_EABI_TOOLCHAIN_PATH=<path to the ARM toolchain> -DNRF5_SDK_PATH=<path to the NRF SDK> ..
$ make
```

The firmware is built in `src/xiao-lora-water-meter-app-1.0.0.bin`. `.hex` and `.out` files are also generated.

## Integration with Home-Assistant

I used a [Node-RED flow](home-assistant/nodered-flow.json) to retried data from the TTN MQTT server, process them and publish them on the local HA MQTT server.

![](doc/nodered.png)

Then, I added new sensors in `configuration.yaml` : 

```
mqtt:
  sensor:
   - name: "watermeter_usage"
     state_topic: "/watermeter/water_value"
     unit_of_measurement: "L"
     device_class: "water"
     state_class: "total_increasing"
     unique_id: "watermetervalue"
   - name: "watermeter_battery"
     state_topic: "/watermeter/battery"
     unit_of_measurement: "v"
     device_class: "voltage"
     state_class: "measurement"
     unique_id: "watermeterbattery"
```